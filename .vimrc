" not sure this is strictly necessary but better safe than sorry :)
set nocompatible

" custom pathogen location
runtime bundle/vim-pathogen/autoload/pathogen.vim
execute pathogen#infect()

"call pathogen#infect()
" add drupal vim config from drush
:call pathogen#infect('~/.drush/vimrc/bundle/{}')
syntax on
filetype plugin indent on
filetype plugin on
set nocompatible
set nobackup
set nowb
set noswapfile
set nowrap

" trying out code folding
"set nofoldenable
setlocal foldmethod=indent
set foldlevelstart=20

" Proper encoding
set encoding=utf-8

" Change mapleader
let mapleader=","

" syntax highligting
syntax enable
set background=dark
set t_Co=256
colorscheme zenburn

" quiet pls
set visualbell t_vb=

" turn on line numbers
set number

" 4 space softabs default
"set expandtab
"set ts=4
"set sw=4
set tabstop=8 
set softtabstop=0 
set expandtab 
set shiftwidth=4 
set smarttab
set shiftround

" Set some junk
set autoindent " Copy indent from last line when starting new line.
set backspace=indent,eol,start
set cursorline " Highlight current line
set hlsearch " Highlight searches
set ignorecase " Ignore case of searches.
set incsearch " Highlight dynamically as pattern is typed.
set laststatus=2 " Always show status line
set nostartofline " Don't reset cursor to start of line when moving around.
set ruler " Show the cursor position
set showmode " Show the current mode.
set scrolloff=3 " Start scrolling three lines before horizontal border of window.
set title " Show the filename in the window titlebar.
set wildmenu " Hitting TAB in command mode will show possible completions above command line.

" Status Line
" hi User1 guibg=#455354 guifg=fg      ctermbg=238 ctermfg=fg  gui=bold,underline cterm=bold,underline term=bold,underline
" hi User2 guibg=#455354 guifg=#CC4329 ctermbg=238 ctermfg=196 gui=bold           cterm=bold           term=bold
set statusline=[%n]\ %1*%<%.99t%*\ %2*%h%w%m%r%*%y[%{&ff}→%{strlen(&fenc)?&fenc:'No\ Encoding'}]%=%-16(\ L%l,C%c\ %)%P
let g:Powerline_symbols = 'fancy'

" Speed up viewport scrolling
nnoremap <C-e> 3<C-e>
nnoremap <C-y> 3<C-y>

" Toggle show tabs and trailing spaces (,c)
set lcs=tab:›\ ,trail:·,eol:¬,nbsp:_
set fcs=fold:-
nnoremap <silent> <leader>c :set nolist!<CR>

" Search and replace word under cursor (,*)
:nnoremap <leader>* :%s/\<<C-r><C-w>\>//<Left>

" Markdown
augroup mkd
  autocmd BufRead *.mkd  set ai formatoptions=tcroqn2 comments=n:>
augroup END

" JSON
au BufRead,BufNewFile *.json set ft=json syntax=javascript

" Rainbow Parenthesis
"nnoremap <leader>rp :RainbowParenthesesToggle<CR>

" Taglist Plus
let Tlist_Ctags_Cmd='/usr/local/Cellar/ctags/5.8/bin/'
let Tlist_WinWidth='auto'
nnoremap <leader>l :TlistToggle<CR>

" ,+n toggles the nerdtree
map <leader>n :NERDTreeToggle<CR>

" show hidden files in nerdtree
let NERDTreeShowHidden=1

" ; to trigger : in normal
nmap ; :

" jj to exit insert mode
imap jj <Esc>

" nice bindings for fugitive
nmap <leader>gs :Gstatus<cr>
nmap <leader>gc :Gcommit<cr>
nmap <leader>ga :Gwrite<cr>
nmap <leader>gl :Glog<cr>
nmap <leader>gd :Gdiff<cr>
nmap <leader>gb :Gdiff<cr>

" set previewHeight window size larger
set previewheight=30

" binding to start grunt server using dispatch
nmap <leader>gg :Start grunt<cr>
" binding to start mongo using dispatch
nmap <leader>mm :Start meteor<cr>
nmap <leader>mo :Start mongo<cr>

let g:symfony_app_console_caller= "php"
let g:symfony_app_console_path= "app/console"

let g:symfony_enable_shell_mapping = 0 "disable the mapping of symfony console

" Use your key instead of default key which is <C-F>
map <leader>sf :execute ":!"g:symfony_enable_shell_cmd<CR>

au BufNewFile,BufRead *.twig, set ft=jinja
au BufNewFile,BufRead *.html,*.htm,*.shtml,*.stm,*.mustache set ft=html

set pastetoggle=<F10>
set pastetoggle=<leader>p

" allow jsx hightlighting on regular js files
let g:jsx_ext_required = 0

" make sure the cursor doesn't jump around when yanking
vnoremap y myy`y
vnoremap Y myY`y

" Add ^ functionality to 0 on double tap
function! ToggleMovement(firstOp, thenOp)
    let pos = getpos('.')
    execute "normal! " . a:firstOp
    if pos == getpos('.')
        execute "normal! " . a:thenOp
    endif
endfunction

" The original carat 0 swap
nnoremap <silent> 0 :call ToggleMovement('^', '0')<CR>
