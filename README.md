dotfiles
========

Default dotfiles config. 

On OSX we need to update Vim using Homebrew:

```
brew install macvim
```
```
brew install cmake
git submodule update --init --recursive
./vim/bundle/YouCompleteMe/install.py
```

Install [the silver searcher for nerdtree-ag](https://github.com/ggreer/the_silver_searcher)

```
brew install the_silver_searcher
```

Install [thefuck](https://github.com/nvbn/thefuck)

```
wget -O - https://raw.githubusercontent.com/nvbn/thefuck/master/install.sh | sh - && $0
```