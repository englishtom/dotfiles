export ZSH=$HOME/.oh-my-zsh
export ZSH_THEME="robbyrussell"
plugins=(osx git ruby gem rails brew pow)
source $ZSH/oh-my-zsh.sh

[[ -s $HOME/.tmuxinator/scripts/tmuxinator ]] && source $HOME/.tmuxinator/scripts/tmuxinator

# Customize to your needs...
export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/racket/bin:$PATH

# alias vim to the macvim binary
alias vim="/Applications/MacVim.app/Contents/MacOS/Vim"
eval $(thefuck --alias)

# copy working directory
alias cwd="pwd | pbcopy"
